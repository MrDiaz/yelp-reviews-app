//
//  Yelp_Reviews_AppTests.swift
//  Yelp Reviews AppTests
//
//  Created by Manuel Diaz on 2017-11-15.
//  Copyright © 2017 Manuel Diaz. All rights reserved.
//

import XCTest
@testable import Yelp_Reviews_App

class Yelp_Reviews_AppTests: XCTestCase {
    
    var sampleBusiness = Business(identifier: "12",
                                  name: "La Cubana",
                                  price: "#",
                                  imageURL: URL(string: "www.google.com")!,
                                  phone: "4169995723",
                                  reviewCount: 123,
                                  photos: nil,
                                  reviews: [Review(rating: 4,
                                                  text: "This is the most amazing review ever")],
                                  location: BusinessLocation(address: "77 Showcase Drive",
                                                             country: "Canada",
                                                             city: "Hamilton"))
    
    var viewModel: BusinessViewModel!
    
    override func setUp() {
        super.setUp()
        self.viewModel = BusinessViewModel(business: sampleBusiness)
    }
    
    func testViewModel() {
        XCTAssertEqual(viewModel.location, "77 Showcase Drive, Hamilton")
        XCTAssertEqual(viewModel.reviews, "123 reviews")
    }
    
}
