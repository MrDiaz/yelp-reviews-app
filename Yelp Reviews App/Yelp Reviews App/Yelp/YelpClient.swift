//
//  YelpClient.swift
//  Yelp Reviews App
//
//  Created by Manuel Diaz on 2017-11-15.
//  Copyright © 2017 Manuel Diaz. All rights reserved.
//

import Foundation

struct SearchResponse: Codable {
    let total: UInt
    let businesses: [Business]
}

struct ReviewResponse: Codable {
    let reviews: [Review]
}

class YelpClient {
    
    private let appID: String
    private let secret: String
    private var accessToken: String
    
    init(appID: String, secret: String) {
        self.appID = appID
        self.secret = secret
        self.accessToken = ""
    }
    
    /**
     Authorize the client with Yelp's API.
     - parameter completion: True if authentication was successful
     */
    func authorize(completion: @escaping (Bool) -> ()) {
        let request = self.authenticationRequest()
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { (data, response, error) in
            guard error == nil else {
                completion(false)
                return
            }
            
            if let JSONObject = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String: AnyObject] {
                if let token = JSONObject["access_token"] as? String {
                    self.accessToken = token
                }
                completion(true)
            }
        })
        task.resume()
    }
    
    /**
    Search for restaurants in a given location
     - parameter location: the location where to search
     - paramter limit: max number of results
     - parameter completion: the completion block
     */
    func search(withLocation location: String, limit: UInt = 10, sort: String, term: String, completion: @escaping (SearchResponse?) -> ()) {
        var queryParams = [URLQueryItem]()
        queryParams.append(URLQueryItem(name: "limit", value: "\(limit)"))
        queryParams.append(URLQueryItem(name: "sort", value: sort))
        queryParams.append(URLQueryItem(name: "term", value: term))
        queryParams.append(URLQueryItem(name: "location", value: location))
        let request = self.request(withPath: "/v3/businesses/search", parameters: queryParams)
        let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
            guard error == nil else {
                completion(nil)
                return
            }
            
            if let searchResult = try? JSONDecoder().decode(SearchResponse.self, from: data!) {
               completion(searchResult)
            }
        })
        task.resume()
    }
    
    func reviews(forBusiness identifier: String, completion: @escaping (ReviewResponse?) -> ()) {
        let request = self.request(withPath: "/v3/businesses/\(identifier)/reviews", parameters: nil)
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard error == nil else {
                completion(nil)
                return
            }
            
            if let reviewResult = try? JSONDecoder().decode(ReviewResponse.self, from: data!) {
                completion(reviewResult)
            }
        }
        task.resume()
    }
    
    func business(bussinessId: String, completion: @escaping (Business?) -> ()) {
        let request = self.request(withPath: "/v3/businesses/\(bussinessId)", parameters: nil)
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard error == nil else {
                completion(nil)
                return
            }
            
            if let business = try? JSONDecoder().decode(Business.self, from: data!) {
                completion(business)
            }
        }
        task.resume()
    }
    
    /** Note: I would prefer to use a networking library like AFNetworking but
     I wanted to refrain from using multiple dependencies for demo purposes
     */
    private func authenticationRequest() -> URLRequest {
        
        var components = URLComponents()
        components.scheme = "https"
        components.host = "api.yelp.com"
        components.path = "/oauth2/token"
        
        let body = "grant_type=client_credentials&client_id=\(appID)&client_secret=\(secret)"
        var request = URLRequest(url: components.url!)
        request.httpMethod = "POST"
        request.httpBody = body.data(using: .utf8)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        return request
    }
    
    private func request(withPath path: String, parameters:[URLQueryItem]?) -> URLRequest {
        var components = URLComponents()
        components.host = "api.yelp.com"
        components.path = path
        components.scheme = "https"
        components.queryItems = parameters
        
        var request = URLRequest(url: components.url!)
        request.httpMethod = "GET"
        request.setValue("Bearer \(self.accessToken)", forHTTPHeaderField: "Authorization")
        return request
    }
    
}
