/**
 
    AppCoordinator is responsible for bootstrapping the initialization of the application
 
 */

import UIKit

struct AppCoordinator {
    
    private let navigationController: UINavigationController
    private let window: UIWindow
    private let initialViewController: FeedCollectionViewController
    
    private let client = YelpClient(appID: "qGCPb8GBmb0QtH6TBD54tg", secret: "X5s3uymRS9cx7NPlymdAD51CoPwrQViQWjr8TYXnr1F2dBawIT7830nyIuTad40x")
    
    init(window: UIWindow) {
        self.navigationController = UINavigationController()
        self.window = window
        self.initialViewController = FeedCollectionViewController(viewModel: FeedViewModel(client: client))
    }
    
    func start() {
        window.backgroundColor = .white
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
        
        authenticateYelp()
        styleNavigation()
    }
    
    private func authenticateYelp() {
        client.authorize { success in
            guard success == true else {
                return
            }
            DispatchQueue.main.async {
                self.navigationController.pushViewController(self.initialViewController, animated: true)
                self.initialViewController.handle = { business in
                    self.navigationController.pushViewController(BusinessViewController(viewModel: BusinessDetailViewModel(viewModel: business, client: self.client)), animated: true)
                }
            }
        }
    }
    
    // let's apply some decent styling to the navigation bar
    private func styleNavigation() {
        let navigationBarAppearance = UINavigationBar.appearance()
        navigationBarAppearance.barTintColor = UIColor(named: "Navigation Color")
        navigationBarAppearance.tintColor = .white
        navigationBarAppearance.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]

        UIApplication.shared.statusBarStyle = .lightContent
    }
    
}
