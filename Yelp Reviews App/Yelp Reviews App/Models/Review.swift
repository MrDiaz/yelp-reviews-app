//
//  Review.swift
//  Yelp Reviews App
//
//  Created by Manuel Diaz on 2017-11-16.
//  Copyright © 2017 Manuel Diaz. All rights reserved.
//

import Foundation

struct Review: Codable {
    let rating: UInt
    let text: String
}
