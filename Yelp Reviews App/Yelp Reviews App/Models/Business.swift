//
//  Business.swift
//  Yelp Reviews App
//
//  Created by Manuel Diaz on 2017-11-16.
//  Copyright © 2017 Manuel Diaz. All rights reserved.
//

import Foundation

struct Business: Codable {
    let identifier: String
    let name: String
    let price: String?
    let imageURL: URL
    let phone: String
    let reviewCount: UInt
    let photos: [URL]?
    let reviews: [Review]?
    let location: BusinessLocation?
    
    enum CodingKeys: String, CodingKey {
        case identifier = "id"
        case name = "name"
        case imageURL = "image_url"
        case price = "price"
        case phone = "phone"
        case reviewCount = "review_count"
        case photos = "photos"
        case reviews = "reviews"
        case location = "location"
    }
}

struct BusinessLocation: Codable {
    let address: String
    let country: String
    let city: String
    
    enum CodingKeys: String, CodingKey {
        case address = "address1"
        case country = "country"
        case city = "city"
    }
}

