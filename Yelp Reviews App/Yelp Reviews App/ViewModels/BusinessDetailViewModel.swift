//
//  BusinessDetailViewModel.swift
//  Yelp Reviews App
//
//  Created by Manuel Diaz on 2017-11-16.
//  Copyright © 2017 Manuel Diaz. All rights reserved.
//

import Foundation

struct BusinessDetailViewModel {
    
    private let client: YelpClient
    private let viewModel: BusinessViewModel
    
    let title: String
    let phone: String
    let price: String
    let reviews: String
    
    init(viewModel: BusinessViewModel, client: YelpClient) {
        self.viewModel = viewModel
        self.client = client
        
        self.title = viewModel.title
        self.phone = "Call \(viewModel.phone)"
        self.reviews = viewModel.reviews
        self.price = viewModel.price
    }
    
    func getLatestReview(completion: @escaping (Review?) -> ()) {
        client.reviews(forBusiness: viewModel.identifier) { response in
            DispatchQueue.main.async {
                completion(response?.reviews[0])
            }
        }
    }
    
    func getPhotos(completion: @escaping ([URL]?) -> ()) {
        client.business(bussinessId: viewModel.identifier) { business in
            DispatchQueue.main.async {
                completion(business?.photos)
            }
        }
    }
}
