//
//  BusinessViewModel.swift
//  Yelp Reviews App
//
//  Created by Manuel Diaz on 2017-11-16.
//  Copyright © 2017 Manuel Diaz. All rights reserved.
//

import Foundation

struct BusinessViewModel {
    let title: String
    let price: String
    let imageURL: URL
    let reviews: String
    let phone: String
    let identifier: String
    let location: String
    
    init(business: Business) {
        self.identifier = business.identifier
        self.title = business.name
        self.price = business.price ?? ""
        self.imageURL = business.imageURL
        self.reviews = "\(business.reviewCount) reviews"
        self.phone = business.phone
        
        if let location = business.location {
            self.location = "\(location.address), \(location.city)"
        } else {
            self.location = ""
        }
    }
}
