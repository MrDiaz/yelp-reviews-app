//
//  FeedViewModel.swift
//  Yelp Reviews App
//
//  Created by Manuel Diaz on 2017-11-15.
//  Copyright © 2017 Manuel Diaz. All rights reserved.
//

import Foundation

enum Sort: String {
    case best = "best_match"
    case rating = "rating"
    case review = "review_count"
}

protocol FeedViewModelType {
    var title: String { get }
    var sort: Sort { set get }
    var term: String { set get }

    func fetchRestaurants(term: String, sort: Sort, completion: @escaping (Array<BusinessViewModel>?) -> ())
}

struct FeedViewModel: FeedViewModelType {
   
    private let client: YelpClient
    let title = "Yelp Reviews"
    var sort: Sort = .best
    var term: String = "Ethiopian"
    
    init(client: YelpClient) {
        self.client = client
    }
   
    func fetchRestaurants(term: String, sort: Sort, completion: @escaping (Array<BusinessViewModel>?) -> ()) {
        client.search(withLocation: "Toronto", sort: sort.rawValue, term: term) { restaurants in
            completion(restaurants?.businesses.map { BusinessViewModel(business: $0) })
        }
    }
    
}
