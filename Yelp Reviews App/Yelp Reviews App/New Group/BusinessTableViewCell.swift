//
//  RestaurantTableViewCell.swift
//  Yelp Reviews App
//
//  Created by Manuel Diaz on 2017-11-16.
//  Copyright © 2017 Manuel Diaz. All rights reserved.
//

import UIKit
import SDWebImage

class BusinessTableViewCell: UITableViewCell {
    
    @IBOutlet weak private var restaurantImageView: UIImageView!
    @IBOutlet weak private var titleLabel: UILabel!
    @IBOutlet weak private var priceLabel: UILabel!
    @IBOutlet weak private var reviewLabel: UILabel!
    @IBOutlet weak private var locationLabel: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        restaurantImageView.image = nil
    }
    
    func bindViewModel(_ viewModel: BusinessViewModel) {
        titleLabel.text = viewModel.title
        priceLabel.text = viewModel.price
        reviewLabel.text = viewModel.reviews
        locationLabel.text = viewModel.location
        restaurantImageView.sd_setImage(with: viewModel.imageURL, completed: nil)
    }
    
}
