//
//  FeedCollectionViewController.swift
//  Yelp Reviews App
//
//  Created by Manuel Diaz on 2017-11-15.
//  Copyright © 2017 Manuel Diaz. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class FeedCollectionViewController: UITableViewController {
    
    private var viewModel: FeedViewModelType
    private var businesses = [BusinessViewModel]()
    private let searchController = UISearchController(searchResultsController: nil)
    
    typealias DetailHandle = (BusinessViewModel) -> ()
    var handle: DetailHandle?
    
    init(viewModel: FeedViewModelType) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UINib(nibName: "BusinessTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: reuseIdentifier)
        title = viewModel.title
        
        setupFilters()
        setupSearch()
        loadRestaurants(term: viewModel.term, sort: viewModel.sort)
    }
    
    private func setupFilters() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Sort", style: .plain, target: self, action: #selector(filterAction))
    }
    
    private func setupSearch() {
        searchController.searchResultsUpdater = self
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.sizeToFit()
        navigationItem.titleView = searchController.searchBar
    }
    
    @objc private func filterAction() {
        let alertController = UIAlertController(title: "Sort by", message: nil, preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "Best Match", style: .default, handler: { action in
            self.loadRestaurants(term: self.viewModel.term, sort: .best)
        }))
        alertController.addAction(UIAlertAction(title: "Highest Rated", style: .default, handler: { action in
            self.loadRestaurants(term: self.viewModel.term, sort: .rating)
        }))
        alertController.addAction(UIAlertAction(title: "Most Reviews", style: .default, handler: { action in
            self.loadRestaurants(term: self.viewModel.term, sort: .review)
        }))
        present(alertController, animated: true)
    }
    
    private func loadRestaurants(term: String?, sort: Sort) {
        businesses.removeAll()
        tableView.reloadData()
        viewModel.fetchRestaurants(term: term ?? self.viewModel.term, sort: sort) { restaurants in
            self.businesses = restaurants ?? ([])
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
}

extension FeedCollectionViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchString = searchController.searchBar.text
        if let searchTerm = searchString, searchTerm.count > 2 {
            viewModel.term = searchTerm
            loadRestaurants(term: searchTerm, sort: .best)
        }
    }
}

extension FeedCollectionViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return businesses.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! BusinessTableViewCell
        cell.selectionStyle = .none
        cell.bindViewModel(businesses[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 102
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        handle?(businesses[indexPath.row])
    }
}
