//
//  RestaurantViewController.swift
//  Yelp Reviews App
//
//  Created by Manuel Diaz on 2017-11-15.
//  Copyright © 2017 Manuel Diaz. All rights reserved.
//

import UIKit

class BusinessViewController: UIViewController {
    
    private let viewModel: BusinessDetailViewModel
    
    @IBOutlet weak private var nameLabel: UILabel!
    @IBOutlet weak private var scrollView: UIScrollView!
    @IBOutlet weak private var callButton: UIButton!
    @IBOutlet weak private var priceLabel: UILabel!
    @IBOutlet weak private var reviewLabel: UILabel!
    @IBOutlet weak private var textView: UITextView!
    @IBOutlet weak private var pageControl: UIPageControl!
    
    init(viewModel: BusinessDetailViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: Bundle.main)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        nameLabel.text = viewModel.title
        callButton.setTitle(viewModel.phone, for: .normal)
        priceLabel.text = viewModel.price
        reviewLabel.text = viewModel.reviews
        
        viewModel.getLatestReview { review in
            self.textView.text = review?.text
        }
        
        viewModel.getPhotos { photoURLs in
            self.setPhotos(photos: photoURLs ?? [])
        }
    }
    
    // Add photos to the view controller
    private func setPhotos(photos: [URL]) {
        var xPos: CGFloat = 0
        pageControl.numberOfPages = photos.count
        scrollView.contentSize = CGSize(width: scrollView.bounds.width * CGFloat(photos.count), height: scrollView.bounds.height)
        for i in 0...photos.count - 1 {
            let imageView = UIImageView(frame: CGRect(x: xPos, y: 0, width: scrollView.bounds.width, height: scrollView.bounds.height))
            imageView.sd_setImage(with: photos[i], completed: nil)
            self.scrollView.addSubview(imageView)
            xPos += scrollView.bounds.width
        }
    }

    @IBAction func callAction(_ sender: Any) {
        if let url = URL(string: "telprompt:\(viewModel.phone)") {
            UIApplication.shared.open(url)
        }
    }

}

extension BusinessViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentPosition = Double(getCurrentPosition())
        let newPageIndex = Int(round(currentPosition))
        
        if newPageIndex != pageControl.currentPage {
            pageControl.currentPage = newPageIndex
        }
    }
    
    private func getCurrentPosition() -> CGFloat {
        let boundsWidth = scrollView.bounds.width
        let contentOffset = scrollView.contentOffset.x
        let currentPosition = contentOffset / boundsWidth
        return currentPosition
    }
}

